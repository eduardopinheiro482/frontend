# README

## Desafio Full stack (Quarantine.cult)

# Requirements

    ember ~ 3.9
    ember-paper ~ 1.0.0-beta.20

# Setup frontend

    ember install ember-paper@1.0.0-beta.20
    ember serve

Application should be ready at http://0.0.0.0:4200/