import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('events', {path: '/' });
  this.route('event', function() {
    this.route('new');
    this.route('index', { path: '/:id' });
  });
  this.route('login');
  this.route('users', function() {
    this.route('new');
    this.route('confirm_email', { path: '/:token/confirm_email' })
  });
});

export default Router;
