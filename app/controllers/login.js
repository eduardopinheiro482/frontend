import Controller from '@ember/controller';
import { inject as service } from '@ember/service';

export default Controller.extend({
  router: service(),
  session: service(),

  actions: {
    authenticate: function() {
      const credentials = this.getProperties('email', 'password');
      const authenticator = 'authenticator:token';

      var self = this;

      function loginSuccess() {
        self.get('router').transitionTo('/')
      }

      function loginFailure(reason) {
        alert(reason)
        self.get('router').transitionTo('/login')
      }

      this.get('session').authenticate(authenticator, credentials).then(loginSuccess).catch(loginFailure);
    },
  }
});