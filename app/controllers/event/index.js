import {inject} from '@ember/service';
import { set } from '@ember/object';
import Controller from '@ember/controller';

export default Controller.extend({
  router: inject(),
  actions: {
    submitComment() {
      set(this.model.comment, "name", this.get('name'));
      set(this.model.comment, "email", this.get('email'));
      set(this.model.comment, "comment", this.get('comment_text'));
      set(this.model.comment, "event", this.model.event);

      var self = this;

      function refreshModel() {
        self.refresh();
      }

      function saveFailure(reason) {
        alert(reason)
        self.get('router').transitionTo('event/new');
      }

      // saving and redirecting
      this.model.comment.save().then(refreshModel()).catch(saveFailure);
    }
  }
});
