import {inject} from '@ember/service';
import { set } from '@ember/object';
import Controller from '@ember/controller';

export default Controller.extend({
  router: inject(),
  session: inject(),
  actions: {
    submitEvent() {
      // setting values
      set(this.model, "name", this.get('name'));
      set(this.model, "type", this.get('type'));
      set(this.model, "description", this.get('description'));
      set(this.model, "url", this.get('url'));
      set(this.model, "disponibility", this.get('disponibility'));

      var self = this;

      function saveSuccess(post) {
        self.get('router').transitionTo('event', post.id)
      }

      function saveFailure(reason) {
        alert(reason)
        self.get('router').transitionTo('event/new')
      }

      // saving and redirecting
      this.model.save().then(saveSuccess).catch(saveFailure);
    },
  }
});
