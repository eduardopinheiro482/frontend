import Controller from '@ember/controller';

export default Controller.extend({
  queryParams: ['page', 'size', 'name', "type", "disponibility"],
  page: 1,
  size: 4,
  name: null,
  type: null,
  disponibility: null,
  actions: {
    filterEvents() {
      this.get('router').transitionTo('events',
        {
          queryParams: {
            name: this.name,
            type: this.type,
            disponibility: this.disponibility
          }
        }
      )
    },
    toggle(propName) {
      this.toggleProperty(propName);
    },
  }
});
