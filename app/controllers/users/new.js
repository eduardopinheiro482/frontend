import Controller from '@ember/controller';
import {inject} from '@ember/service';
import { set } from '@ember/object';

export default Controller.extend({
  router: inject(),
  actions: {
    submitUser() {
      // setting values
      set(this.model, "name", this.get('name'));
      set(this.model, "email", this.get('email'));
      set(this.model, "password", this.get('password'));

      var self = this;

      function saveSuccess() {
        self.get('router').transitionTo('login')
      }

      function saveFailure(reason) {
        alert(reason)
        self.get('router').transitionTo('users/new')
      }

      // saving and redirecting
      this.model.save().then(saveSuccess).catch(saveFailure);
    },
  }
});
