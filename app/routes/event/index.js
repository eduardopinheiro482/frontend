import Route from '@ember/routing/route';
import RSVP from 'rsvp';

export default Route.extend({
  model(params) {
    return RSVP.hash({
      event: this.store.findRecord('event', params.id),
      comment: this.store.createRecord('comment')
    });
  }
});
