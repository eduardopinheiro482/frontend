import Route from '@ember/routing/route';

export default Route.extend({

  queryParams: {
    page: {
      refreshModel: true
    },
    size: {
      refreshModel: true
    },
    type: {
      refreshModel: true
    },
    name: {
      refreshModel: true
    },
    disponibility: {
      refreshModel: true
    }
  },

  model(params) {
    return this.store.query('event', {
      page: {
        number: params.page,
        size: params.size
      },
      filter: {
        name: params.name,
        type: params.type,
        disponibility: params.disponibility
      }
    });
  }
});
