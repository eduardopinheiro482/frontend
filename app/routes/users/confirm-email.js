import Route from '@ember/routing/route';
import fetch from 'fetch';
import ENV from '../../config/environment';

export default Route.extend({
  model(params) {
    var endpoint = ENV.APP.host + "/users/" + params.token + '/confirm_email'

    return fetch(endpoint).then(
      function(response) {
        if (!response.ok) {
          throw Error(response.status)
        }
      })
    .then(
      function() {
        return true
      })
    .catch(
      function() {
        return false
      });
  }
});
