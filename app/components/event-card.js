import Component from '@ember/component';
import {inject} from '@ember/service'

export default Component.extend({
  router: inject(),
  actions: {
    showDetails(id) {
      this.get('router').transitionTo('event', id)
    },
  }
});