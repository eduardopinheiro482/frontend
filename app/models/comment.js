import DS from 'ember-data';
const { Model } = DS;

export default Model.extend({
  name: DS.attr(),
  email: DS.attr(),
  comment: DS.attr(),
  created_at: DS.attr('date'),
  updated_at: DS.attr('date'),
  event: DS.belongsTo('event')
});
