import DS from 'ember-data';
const { Model } = DS;

export default Model.extend({
  name: DS.attr(),
  description: DS.attr(),
  type: DS.attr(),
  url: DS.attr(),
  disponibility: DS.attr(),
  comments: DS.hasMany('comment')
});
