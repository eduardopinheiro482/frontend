import DS from 'ember-data';
const { Model } = DS;

export default Model.extend({
  name: DS.attr(),
  email: DS.attr(),
  password: DS.attr()
});
